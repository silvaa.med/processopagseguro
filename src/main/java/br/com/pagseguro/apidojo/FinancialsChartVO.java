package br.com.pagseguro.apidojo;

import java.util.List;

public class FinancialsChartVO{
    public List<YearlyVO> yearly;
    public List<QuarterlyVO> quarterly;
	public List<YearlyVO> getYearly() {
		return yearly;
	}
	public void setYearly(List<YearlyVO> yearly) {
		this.yearly = yearly;
	}
	public List<QuarterlyVO> getQuarterly() {
		return quarterly;
	}
	public void setQuarterly(List<QuarterlyVO> quarterly) {
		this.quarterly = quarterly;
	}
}
