package br.com.pagseguro.apidojo;

public class GetQuotesVO{
    public QuoteResponseVO quoteResponse;

	public QuoteResponseVO getQuoteResponse() {
		return quoteResponse;
	}

	public void setQuoteResponse(QuoteResponseVO quoteResponse) {
		this.quoteResponse = quoteResponse;
	}
	
	public double getRegularMarketPrice(){
		if(quoteResponse == null || quoteResponse.getResult() == null){
			return 0.0;
		}
		
		return quoteResponse.getResult().get(0).getRegularMarketPrice();
	}
}
