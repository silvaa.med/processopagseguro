package br.com.pagseguro.apidojo;

import java.util.List;

public class ResultVO{
    public String language;
    public String region;
    public String quoteType;
    public String quoteSourceName;
    public boolean triggerable;
    public QuoteSummaryVO quoteSummary;
    public String currency;
    public int regularMarketTime;
    public long firstTradeDateMilliseconds;
    public int priceHint;
    public double totalCash;
    public long floatShares;
    public long ebitda;
    public int targetPriceHigh;
    public double targetPriceLow;
    public double targetPriceMean;
    public int targetPriceMedian;
    public double heldPercentInsiders;
    public double heldPercentInstitutions;
    public double regularMarketChange;
    public double regularMarketChangePercent;
    public double regularMarketPrice;
    public double regularMarketDayHigh;
    public String regularMarketDayRange;
    public double regularMarketDayLow;
    public int regularMarketVolume;
    public double regularMarketPreviousClose;
    public double bid;
    public double ask;
    public int bidSize;
    public int askSize;
    public String exchange;
    public String market;
    public String messageBoardId;
    public String fullExchangeName;
    public String shortName;
    public String longName;
    public double regularMarketOpen;
    public int averageDailyVolume3Month;
    public int averageDailyVolume10Day;
    public double beta;
    public double fiftyTwoWeekLowChange;
    public double fiftyTwoWeekLowChangePercent;
    public String fiftyTwoWeekRange;
    public double fiftyTwoWeekHighChange;
    public double fiftyTwoWeekHighChangePercent;
    public double fiftyTwoWeekLow;
    public double fiftyTwoWeekHigh;
    public int exDividendDate;
    public int earningsTimestamp;
    public int earningsTimestampStart;
    public int earningsTimestampEnd;
    public double trailingAnnualDividendRate;
    public double trailingPE;
    public int dividendsPerShare;
    public double dividendRate;
    public double trailingAnnualDividendYield;
    public double dividendYield;
    public double revenue;
    public double priceToSales;
    public String marketState;
    public double epsTrailingTwelveMonths;
    public double epsForward;
    public long sharesOutstanding;
    public double bookValue;
    public double fiftyDayAverage;
    public double fiftyDayAverageChange;
    public double fiftyDayAverageChangePercent;
    public double twoHundredDayAverage;
    public double twoHundredDayAverageChange;
    public double twoHundredDayAverageChangePercent;
    public long marketCap;
    public double forwardPE;
    public double priceToBook;
    public int sourceInterval;
    public int exchangeDataDelayedBy;
    public String exchangeTimezoneName;
    public String exchangeTimezoneShortName;
    public int gmtOffSetMilliseconds;
    public boolean esgPopulated;
    public boolean tradeable;
    public List<String> components;
    public String symbol;
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public String getQuoteSourceName() {
		return quoteSourceName;
	}
	public void setQuoteSourceName(String quoteSourceName) {
		this.quoteSourceName = quoteSourceName;
	}
	public boolean isTriggerable() {
		return triggerable;
	}
	public void setTriggerable(boolean triggerable) {
		this.triggerable = triggerable;
	}
	public QuoteSummaryVO getQuoteSummary() {
		return quoteSummary;
	}
	public void setQuoteSummary(QuoteSummaryVO quoteSummary) {
		this.quoteSummary = quoteSummary;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getRegularMarketTime() {
		return regularMarketTime;
	}
	public void setRegularMarketTime(int regularMarketTime) {
		this.regularMarketTime = regularMarketTime;
	}
	public long getFirstTradeDateMilliseconds() {
		return firstTradeDateMilliseconds;
	}
	public void setFirstTradeDateMilliseconds(long firstTradeDateMilliseconds) {
		this.firstTradeDateMilliseconds = firstTradeDateMilliseconds;
	}
	public int getPriceHint() {
		return priceHint;
	}
	public void setPriceHint(int priceHint) {
		this.priceHint = priceHint;
	}
	public double getTotalCash() {
		return totalCash;
	}
	public void setTotalCash(double totalCash) {
		this.totalCash = totalCash;
	}
	public long getFloatShares() {
		return floatShares;
	}
	public void setFloatShares(long floatShares) {
		this.floatShares = floatShares;
	}
	public long getEbitda() {
		return ebitda;
	}
	public void setEbitda(long ebitda) {
		this.ebitda = ebitda;
	}
	public int getTargetPriceHigh() {
		return targetPriceHigh;
	}
	public void setTargetPriceHigh(int targetPriceHigh) {
		this.targetPriceHigh = targetPriceHigh;
	}
	public double getTargetPriceLow() {
		return targetPriceLow;
	}
	public void setTargetPriceLow(double targetPriceLow) {
		this.targetPriceLow = targetPriceLow;
	}
	public double getTargetPriceMean() {
		return targetPriceMean;
	}
	public void setTargetPriceMean(double targetPriceMean) {
		this.targetPriceMean = targetPriceMean;
	}
	public int getTargetPriceMedian() {
		return targetPriceMedian;
	}
	public void setTargetPriceMedian(int targetPriceMedian) {
		this.targetPriceMedian = targetPriceMedian;
	}
	public double getHeldPercentInsiders() {
		return heldPercentInsiders;
	}
	public void setHeldPercentInsiders(double heldPercentInsiders) {
		this.heldPercentInsiders = heldPercentInsiders;
	}
	public double getHeldPercentInstitutions() {
		return heldPercentInstitutions;
	}
	public void setHeldPercentInstitutions(double heldPercentInstitutions) {
		this.heldPercentInstitutions = heldPercentInstitutions;
	}
	public double getRegularMarketChange() {
		return regularMarketChange;
	}
	public void setRegularMarketChange(double regularMarketChange) {
		this.regularMarketChange = regularMarketChange;
	}
	public double getRegularMarketChangePercent() {
		return regularMarketChangePercent;
	}
	public void setRegularMarketChangePercent(double regularMarketChangePercent) {
		this.regularMarketChangePercent = regularMarketChangePercent;
	}
	public double getRegularMarketPrice() {
		return regularMarketPrice;
	}
	public void setRegularMarketPrice(double regularMarketPrice) {
		this.regularMarketPrice = regularMarketPrice;
	}
	public double getRegularMarketDayHigh() {
		return regularMarketDayHigh;
	}
	public void setRegularMarketDayHigh(double regularMarketDayHigh) {
		this.regularMarketDayHigh = regularMarketDayHigh;
	}
	public String getRegularMarketDayRange() {
		return regularMarketDayRange;
	}
	public void setRegularMarketDayRange(String regularMarketDayRange) {
		this.regularMarketDayRange = regularMarketDayRange;
	}
	public double getRegularMarketDayLow() {
		return regularMarketDayLow;
	}
	public void setRegularMarketDayLow(double regularMarketDayLow) {
		this.regularMarketDayLow = regularMarketDayLow;
	}
	public int getRegularMarketVolume() {
		return regularMarketVolume;
	}
	public void setRegularMarketVolume(int regularMarketVolume) {
		this.regularMarketVolume = regularMarketVolume;
	}
	public double getRegularMarketPreviousClose() {
		return regularMarketPreviousClose;
	}
	public void setRegularMarketPreviousClose(double regularMarketPreviousClose) {
		this.regularMarketPreviousClose = regularMarketPreviousClose;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getAsk() {
		return ask;
	}
	public void setAsk(double ask) {
		this.ask = ask;
	}
	public int getBidSize() {
		return bidSize;
	}
	public void setBidSize(int bidSize) {
		this.bidSize = bidSize;
	}
	public int getAskSize() {
		return askSize;
	}
	public void setAskSize(int askSize) {
		this.askSize = askSize;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getMessageBoardId() {
		return messageBoardId;
	}
	public void setMessageBoardId(String messageBoardId) {
		this.messageBoardId = messageBoardId;
	}
	public String getFullExchangeName() {
		return fullExchangeName;
	}
	public void setFullExchangeName(String fullExchangeName) {
		this.fullExchangeName = fullExchangeName;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getLongName() {
		return longName;
	}
	public void setLongName(String longName) {
		this.longName = longName;
	}
	public double getRegularMarketOpen() {
		return regularMarketOpen;
	}
	public void setRegularMarketOpen(double regularMarketOpen) {
		this.regularMarketOpen = regularMarketOpen;
	}
	public int getAverageDailyVolume3Month() {
		return averageDailyVolume3Month;
	}
	public void setAverageDailyVolume3Month(int averageDailyVolume3Month) {
		this.averageDailyVolume3Month = averageDailyVolume3Month;
	}
	public int getAverageDailyVolume10Day() {
		return averageDailyVolume10Day;
	}
	public void setAverageDailyVolume10Day(int averageDailyVolume10Day) {
		this.averageDailyVolume10Day = averageDailyVolume10Day;
	}
	public double getBeta() {
		return beta;
	}
	public void setBeta(double beta) {
		this.beta = beta;
	}
	public double getFiftyTwoWeekLowChange() {
		return fiftyTwoWeekLowChange;
	}
	public void setFiftyTwoWeekLowChange(double fiftyTwoWeekLowChange) {
		this.fiftyTwoWeekLowChange = fiftyTwoWeekLowChange;
	}
	public double getFiftyTwoWeekLowChangePercent() {
		return fiftyTwoWeekLowChangePercent;
	}
	public void setFiftyTwoWeekLowChangePercent(double fiftyTwoWeekLowChangePercent) {
		this.fiftyTwoWeekLowChangePercent = fiftyTwoWeekLowChangePercent;
	}
	public String getFiftyTwoWeekRange() {
		return fiftyTwoWeekRange;
	}
	public void setFiftyTwoWeekRange(String fiftyTwoWeekRange) {
		this.fiftyTwoWeekRange = fiftyTwoWeekRange;
	}
	public double getFiftyTwoWeekHighChange() {
		return fiftyTwoWeekHighChange;
	}
	public void setFiftyTwoWeekHighChange(double fiftyTwoWeekHighChange) {
		this.fiftyTwoWeekHighChange = fiftyTwoWeekHighChange;
	}
	public double getFiftyTwoWeekHighChangePercent() {
		return fiftyTwoWeekHighChangePercent;
	}
	public void setFiftyTwoWeekHighChangePercent(double fiftyTwoWeekHighChangePercent) {
		this.fiftyTwoWeekHighChangePercent = fiftyTwoWeekHighChangePercent;
	}
	public double getFiftyTwoWeekLow() {
		return fiftyTwoWeekLow;
	}
	public void setFiftyTwoWeekLow(double fiftyTwoWeekLow) {
		this.fiftyTwoWeekLow = fiftyTwoWeekLow;
	}
	public double getFiftyTwoWeekHigh() {
		return fiftyTwoWeekHigh;
	}
	public void setFiftyTwoWeekHigh(double fiftyTwoWeekHigh) {
		this.fiftyTwoWeekHigh = fiftyTwoWeekHigh;
	}
	public int getExDividendDate() {
		return exDividendDate;
	}
	public void setExDividendDate(int exDividendDate) {
		this.exDividendDate = exDividendDate;
	}
	public int getEarningsTimestamp() {
		return earningsTimestamp;
	}
	public void setEarningsTimestamp(int earningsTimestamp) {
		this.earningsTimestamp = earningsTimestamp;
	}
	public int getEarningsTimestampStart() {
		return earningsTimestampStart;
	}
	public void setEarningsTimestampStart(int earningsTimestampStart) {
		this.earningsTimestampStart = earningsTimestampStart;
	}
	public int getEarningsTimestampEnd() {
		return earningsTimestampEnd;
	}
	public void setEarningsTimestampEnd(int earningsTimestampEnd) {
		this.earningsTimestampEnd = earningsTimestampEnd;
	}
	public double getTrailingAnnualDividendRate() {
		return trailingAnnualDividendRate;
	}
	public void setTrailingAnnualDividendRate(double trailingAnnualDividendRate) {
		this.trailingAnnualDividendRate = trailingAnnualDividendRate;
	}
	public double getTrailingPE() {
		return trailingPE;
	}
	public void setTrailingPE(double trailingPE) {
		this.trailingPE = trailingPE;
	}
	public int getDividendsPerShare() {
		return dividendsPerShare;
	}
	public void setDividendsPerShare(int dividendsPerShare) {
		this.dividendsPerShare = dividendsPerShare;
	}
	public double getDividendRate() {
		return dividendRate;
	}
	public void setDividendRate(double dividendRate) {
		this.dividendRate = dividendRate;
	}
	public double getTrailingAnnualDividendYield() {
		return trailingAnnualDividendYield;
	}
	public void setTrailingAnnualDividendYield(double trailingAnnualDividendYield) {
		this.trailingAnnualDividendYield = trailingAnnualDividendYield;
	}
	public double getDividendYield() {
		return dividendYield;
	}
	public void setDividendYield(double dividendYield) {
		this.dividendYield = dividendYield;
	}
	public double getRevenue() {
		return revenue;
	}
	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}
	public double getPriceToSales() {
		return priceToSales;
	}
	public void setPriceToSales(double priceToSales) {
		this.priceToSales = priceToSales;
	}
	public String getMarketState() {
		return marketState;
	}
	public void setMarketState(String marketState) {
		this.marketState = marketState;
	}
	public double getEpsTrailingTwelveMonths() {
		return epsTrailingTwelveMonths;
	}
	public void setEpsTrailingTwelveMonths(double epsTrailingTwelveMonths) {
		this.epsTrailingTwelveMonths = epsTrailingTwelveMonths;
	}
	public double getEpsForward() {
		return epsForward;
	}
	public void setEpsForward(double epsForward) {
		this.epsForward = epsForward;
	}
	public long getSharesOutstanding() {
		return sharesOutstanding;
	}
	public void setSharesOutstanding(long sharesOutstanding) {
		this.sharesOutstanding = sharesOutstanding;
	}
	public double getBookValue() {
		return bookValue;
	}
	public void setBookValue(double bookValue) {
		this.bookValue = bookValue;
	}
	public double getFiftyDayAverage() {
		return fiftyDayAverage;
	}
	public void setFiftyDayAverage(double fiftyDayAverage) {
		this.fiftyDayAverage = fiftyDayAverage;
	}
	public double getFiftyDayAverageChange() {
		return fiftyDayAverageChange;
	}
	public void setFiftyDayAverageChange(double fiftyDayAverageChange) {
		this.fiftyDayAverageChange = fiftyDayAverageChange;
	}
	public double getFiftyDayAverageChangePercent() {
		return fiftyDayAverageChangePercent;
	}
	public void setFiftyDayAverageChangePercent(double fiftyDayAverageChangePercent) {
		this.fiftyDayAverageChangePercent = fiftyDayAverageChangePercent;
	}
	public double getTwoHundredDayAverage() {
		return twoHundredDayAverage;
	}
	public void setTwoHundredDayAverage(double twoHundredDayAverage) {
		this.twoHundredDayAverage = twoHundredDayAverage;
	}
	public double getTwoHundredDayAverageChange() {
		return twoHundredDayAverageChange;
	}
	public void setTwoHundredDayAverageChange(double twoHundredDayAverageChange) {
		this.twoHundredDayAverageChange = twoHundredDayAverageChange;
	}
	public double getTwoHundredDayAverageChangePercent() {
		return twoHundredDayAverageChangePercent;
	}
	public void setTwoHundredDayAverageChangePercent(double twoHundredDayAverageChangePercent) {
		this.twoHundredDayAverageChangePercent = twoHundredDayAverageChangePercent;
	}
	public long getMarketCap() {
		return marketCap;
	}
	public void setMarketCap(long marketCap) {
		this.marketCap = marketCap;
	}
	public double getForwardPE() {
		return forwardPE;
	}
	public void setForwardPE(double forwardPE) {
		this.forwardPE = forwardPE;
	}
	public double getPriceToBook() {
		return priceToBook;
	}
	public void setPriceToBook(double priceToBook) {
		this.priceToBook = priceToBook;
	}
	public int getSourceInterval() {
		return sourceInterval;
	}
	public void setSourceInterval(int sourceInterval) {
		this.sourceInterval = sourceInterval;
	}
	public int getExchangeDataDelayedBy() {
		return exchangeDataDelayedBy;
	}
	public void setExchangeDataDelayedBy(int exchangeDataDelayedBy) {
		this.exchangeDataDelayedBy = exchangeDataDelayedBy;
	}
	public String getExchangeTimezoneName() {
		return exchangeTimezoneName;
	}
	public void setExchangeTimezoneName(String exchangeTimezoneName) {
		this.exchangeTimezoneName = exchangeTimezoneName;
	}
	public String getExchangeTimezoneShortName() {
		return exchangeTimezoneShortName;
	}
	public void setExchangeTimezoneShortName(String exchangeTimezoneShortName) {
		this.exchangeTimezoneShortName = exchangeTimezoneShortName;
	}
	public int getGmtOffSetMilliseconds() {
		return gmtOffSetMilliseconds;
	}
	public void setGmtOffSetMilliseconds(int gmtOffSetMilliseconds) {
		this.gmtOffSetMilliseconds = gmtOffSetMilliseconds;
	}
	public boolean isEsgPopulated() {
		return esgPopulated;
	}
	public void setEsgPopulated(boolean esgPopulated) {
		this.esgPopulated = esgPopulated;
	}
	public boolean isTradeable() {
		return tradeable;
	}
	public void setTradeable(boolean tradeable) {
		this.tradeable = tradeable;
	}
	public List<String> getComponents() {
		return components;
	}
	public void setComponents(List<String> components) {
		this.components = components;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}

