package br.com.pagseguro.apidojo;

public class YearlyVO{
    public int date;
    public Object revenue;
    public Object earnings;
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public Object getRevenue() {
		return revenue;
	}
	public void setRevenue(Object revenue) {
		this.revenue = revenue;
	}
	public Object getEarnings() {
		return earnings;
	}
	public void setEarnings(Object earnings) {
		this.earnings = earnings;
	}
}
