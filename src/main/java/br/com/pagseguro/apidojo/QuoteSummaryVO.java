package br.com.pagseguro.apidojo;

public class QuoteSummaryVO{
    public EarningsVO earnings;

	public EarningsVO getEarnings() {
		return earnings;
	}

	public void setEarnings(EarningsVO earnings) {
		this.earnings = earnings;
	}
}
