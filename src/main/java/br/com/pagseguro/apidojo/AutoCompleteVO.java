package br.com.pagseguro.apidojo;

import java.util.List;

public class AutoCompleteVO{
    public List<Object> explains;
    public int count;
    public List<QuoteVO> quotes;
    public List<NewsVO> news;
    public List<Object> nav;
    public List<Object> lists;
    public List<Object> researchReports;
    public int totalTime;
    public int timeTakenForQuotes;
    public int timeTakenForNews;
    public int timeTakenForAlgowatchlist;
    public int timeTakenForPredefinedScreener;
    public int timeTakenForCrunchbase;
    public int timeTakenForNav;
    public int timeTakenForResearchReports;
	public List<Object> getExplains() {
		return explains;
	}
	public void setExplains(List<Object> explains) {
		this.explains = explains;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<QuoteVO> getQuotes() {
		return quotes;
	}
	public void setQuotes(List<QuoteVO> quotes) {
		this.quotes = quotes;
	}
	public List<NewsVO> getNews() {
		return news;
	}
	public void setNews(List<NewsVO> news) {
		this.news = news;
	}
	public List<Object> getNav() {
		return nav;
	}
	public void setNav(List<Object> nav) {
		this.nav = nav;
	}
	public List<Object> getLists() {
		return lists;
	}
	public void setLists(List<Object> lists) {
		this.lists = lists;
	}
	public List<Object> getResearchReports() {
		return researchReports;
	}
	public void setResearchReports(List<Object> researchReports) {
		this.researchReports = researchReports;
	}
	public int getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(int totalTime) {
		this.totalTime = totalTime;
	}
	public int getTimeTakenForQuotes() {
		return timeTakenForQuotes;
	}
	public void setTimeTakenForQuotes(int timeTakenForQuotes) {
		this.timeTakenForQuotes = timeTakenForQuotes;
	}
	public int getTimeTakenForNews() {
		return timeTakenForNews;
	}
	public void setTimeTakenForNews(int timeTakenForNews) {
		this.timeTakenForNews = timeTakenForNews;
	}
	public int getTimeTakenForAlgowatchlist() {
		return timeTakenForAlgowatchlist;
	}
	public void setTimeTakenForAlgowatchlist(int timeTakenForAlgowatchlist) {
		this.timeTakenForAlgowatchlist = timeTakenForAlgowatchlist;
	}
	public int getTimeTakenForPredefinedScreener() {
		return timeTakenForPredefinedScreener;
	}
	public void setTimeTakenForPredefinedScreener(int timeTakenForPredefinedScreener) {
		this.timeTakenForPredefinedScreener = timeTakenForPredefinedScreener;
	}
	public int getTimeTakenForCrunchbase() {
		return timeTakenForCrunchbase;
	}
	public void setTimeTakenForCrunchbase(int timeTakenForCrunchbase) {
		this.timeTakenForCrunchbase = timeTakenForCrunchbase;
	}
	public int getTimeTakenForNav() {
		return timeTakenForNav;
	}
	public void setTimeTakenForNav(int timeTakenForNav) {
		this.timeTakenForNav = timeTakenForNav;
	}
	public int getTimeTakenForResearchReports() {
		return timeTakenForResearchReports;
	}
	public void setTimeTakenForResearchReports(int timeTakenForResearchReports) {
		this.timeTakenForResearchReports = timeTakenForResearchReports;
	}
	
	public String getSymbol(){
		if(quotes == null){
			return "";
		}
		return quotes.get(0).getSymbol();
	}
}
