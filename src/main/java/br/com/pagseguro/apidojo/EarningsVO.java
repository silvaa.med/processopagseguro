package br.com.pagseguro.apidojo;

public class EarningsVO{
    public int maxAge;
    public EarningsChartVO earningsChart;
    public FinancialsChartVO financialsChart;
    public String financialCurrency;
	public int getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	public EarningsChartVO getEarningsChart() {
		return earningsChart;
	}
	public void setEarningsChart(EarningsChartVO earningsChart) {
		this.earningsChart = earningsChart;
	}
	public FinancialsChartVO getFinancialsChart() {
		return financialsChart;
	}
	public void setFinancialsChart(FinancialsChartVO financialsChart) {
		this.financialsChart = financialsChart;
	}
	public String getFinancialCurrency() {
		return financialCurrency;
	}
	public void setFinancialCurrency(String financialCurrency) {
		this.financialCurrency = financialCurrency;
	}
}
