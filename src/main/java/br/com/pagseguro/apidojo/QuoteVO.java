package br.com.pagseguro.apidojo;

public class QuoteVO{
    public String exchange;
    public String shortname;
    public String quoteType;
    public String symbol;
    public String index;
    public int score;
    public String typeDisp;
    public String longname;
    public boolean isYahooFinance;
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getTypeDisp() {
		return typeDisp;
	}
	public void setTypeDisp(String typeDisp) {
		this.typeDisp = typeDisp;
	}
	public String getLongname() {
		return longname;
	}
	public void setLongname(String longname) {
		this.longname = longname;
	}
	public boolean isYahooFinance() {
		return isYahooFinance;
	}
	public void setYahooFinance(boolean isYahooFinance) {
		this.isYahooFinance = isYahooFinance;
	}
}