package br.com.pagseguro.apidojo;

import java.util.List;

public class QuoteResponseVO{
    public List<ResultVO> result;
    public Object error;
	public List<ResultVO> getResult() {
		return result;
	}
	public void setResult(List<ResultVO> result) {
		this.result = result;
	}
	public Object getError() {
		return error;
	}
	public void setError(Object error) {
		this.error = error;
	}
}
