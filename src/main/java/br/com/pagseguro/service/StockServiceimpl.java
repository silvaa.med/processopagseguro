package br.com.pagseguro.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import br.com.pagseguro.apidojo.AutoCompleteVO;
import br.com.pagseguro.apidojo.GetQuotesVO;
import br.com.pagseguro.dao.StockDao;
import br.com.pagseguro.model.Stock;

@Service
public class StockServiceimpl implements StockService {
	
	private static String KEY = "db679a351emsha00084069b0fe74p12752djsn28e2ac2ac687";
	private static String HOST = "apidojo-yahoo-finance-v1.p.rapidapi.com";
	private static String CONTEXT = "https://apidojo-yahoo-finance-v1.p.rapidapi.com";

	@Autowired
	StockDao dao;
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public void createStock(List<Stock> stocks) {
		dao.saveAll(stocks);
	}

	@Override
	public Collection<Stock> getAllStocks() {
		return dao.findAll();
	}

	@Override
	public Optional<Stock> findStockById(String id) {
		return dao.findById(id);
	}

	@Override
	public void deleteStockById(String id) {
		dao.deleteById(id);
	}

	@Override
	public void updateStock(Stock stock) {
		dao.save(stock);
	}

	@Override
	public void deleteAllStocks() {
		dao.deleteAll();
	}
	
	@Override
	public Stock findStockByCode(String code) {
		return dao.findStockByCode(code);
	}
	
	@Override
	public Stock findStockByName(String name) {
		return dao.findStockByName(name);
	}
	
	@Override
	public void updateSymbols(){
		Collection<Stock> stocks = getAllStocks();
		
		//Atualiza uma vez só os symbols para evitar ter que buscar toda vez.
		for(Stock s : stocks){
			s.setSymbol(getSymbol(s.getCode()));
			updateStock(s);
		}
	}
	
	@Override
	public Collection<Stock> getTopTen() {
		Collection<Stock> stocks = dao.getTopTen();
		
		for(Stock s : stocks){
			s.setRegularMarketPrice(getRegularMarketPrice(s.getSymbol()));
		}
		
		return stocks;
	}
	
	private String getSymbol(String code) {
		if(StringUtils.isEmpty(code)){
			return "";
		}
		HttpEntity<String> entity = getHeaders();
		String url = CONTEXT + "/auto-complete?region=BR&q=" + code;
		ResponseEntity<AutoCompleteVO> response = restTemplate.exchange(url, HttpMethod.GET, entity, AutoCompleteVO.class);        
        return response.getBody().getSymbol();
	}
	
	private double getRegularMarketPrice(String symbol) {
		if(StringUtils.isEmpty(symbol)){
			return 0.0;
		}
		HttpEntity<String> entity = getHeaders();
		String url = CONTEXT + "/market/v2/get-quotes?region=BR&symbols=" + symbol;
		ResponseEntity<GetQuotesVO> response2 = restTemplate.exchange(url, HttpMethod.GET, entity, GetQuotesVO.class);     
        return response2.getBody().getRegularMarketPrice();
	}
	
	private HttpEntity<String> getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("x-rapidapi-key", KEY);
		headers.set("x-rapidapi-host", HOST);
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		return entity;
	}
}