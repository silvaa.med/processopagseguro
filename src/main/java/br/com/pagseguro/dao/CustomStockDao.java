package br.com.pagseguro.dao;

import java.util.Collection;

import org.springframework.stereotype.Repository;

import br.com.pagseguro.model.Stock;

@Repository
public interface CustomStockDao{
	
	public Collection<Stock> getTopTen();
}
