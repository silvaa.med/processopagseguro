FROM openjdk

WORKDIR /app

COPY target/processo-pag-seguro-0.0.1-SNAPSHOT.jar /app/processo-pag-seguro.jar

ENTRYPOINT ["java", "-jar", "processo-pag-seguro.jar"] 